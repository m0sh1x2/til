package mysvc

import "errors"

var ErrNotFound = errors.New("not found")

type User struct {
	ID   int64
	Name string
}

type Service interface {
	GetUser(id int64) (User, error)
	GetUsers(ids []int64) (map[int64]User, error)
}
